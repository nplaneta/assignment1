import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.scene.control.Label
import javafx.scene.control.SplitPane
import javafx.scene.control.Button;
import javafx.scene.control.Separator
import javafx.scene.control.TableColumn
import javafx.scene.control.TableView
import javafx.scene.control.cell.PropertyValueFactory
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import java.io.File
import javafx.scene.input.MouseButton;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import java.util.*

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import java.io.FileNotFoundException
import javafx.scene.control.TextArea
import javafx.scene.control.ScrollPane
import javafx.scene.control.MenuItem
import javafx.scene.Scene
import javafx.stage.Stage
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.stage.FileChooser;
import javafx.stage.DirectoryChooser;
import java.nio.file.Paths

import javafx.scene.text.TextAlignment;
import javafx.geometry.Pos
import javafx.scene.layout.VBox
import javafx.scene.layout.HBox


























// This is the controller class that handles input for our FXML form
// We specify this class in the FXML file when we create it.


class FileWrapper {
    var fileName: File? = null
    var printablename: String? = null

    constructor()
    constructor(fileName: File?, printablename: String?) {
        this.fileName = fileName
        this.printablename = printablename
    }
}

class FXMLController {

    private var model: Model ?= null


    @FXML
    lateinit var label : Label  // lateinit b/c initialized by framework

    @FXML
    lateinit var directoryPathLabel : Label;

    @FXML
    lateinit var directoryTable: TableView<FileWrapper>

    @FXML
    lateinit var fileViewPane: AnchorPane

    @FXML
    lateinit var homeButton: Button

    @FXML
    lateinit var nextButton: Button

    @FXML
    lateinit var prevButton: Button

    @FXML
    lateinit var nextMenuItem: MenuItem

    @FXML
    lateinit var prevMenuItem: MenuItem

    @FXML
    lateinit var renameButton: Button

    @FXML
    lateinit var renameMenuItem: MenuItem

    @FXML
    lateinit var moveButton: Button

    @FXML
    lateinit var moveMenuItem: MenuItem

    @FXML
    lateinit var deleteButton: Button

    @FXML
    lateinit var deleteMenuItem: MenuItem

//
//    @FXML
//    private Font x31;
//
//    @FXML
//    private Color

    var dir = File("${System.getProperty("user.dir")}/test/")

    val column1 = TableColumn<FileWrapper, String>("In '" + dir.name + "' Folder")

    fun initialize() {

        // editing anything about the directorytable MUST be done in this init
        //  Expecting member declaration error given otherwise
        directoryTable.setEditable(true)
        directoryTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);  // Need this so column takes all of table space

        column1.setCellValueFactory(
            PropertyValueFactory("printablename")
        )

        directoryTable.columns.add(column1)



        dir.listFiles().forEach {


                var namevar = it.name

                if(it.isDirectory()) {
                    namevar = "$namevar/"
                }
                directoryTable.getItems().add(FileWrapper(it, namevar))
        }

        directoryTable.setOnMouseClicked { event: MouseEvent ->
            if (event.getButton().equals(MouseButton.PRIMARY) && directoryTable.getSelectionModel().getSelectedItem() != null) {
                setFileDisplay()
                if(event.getClickCount() == 2){
                    var checkifDirectory: Boolean? = directoryTable.getSelectionModel().getSelectedItem().fileName?.isDirectory()
                    if(checkifDirectory == true) {
                        resetDirectoryTable(directoryTable.getSelectionModel().getSelectedItem().fileName)
                    }
                }
            }
        }

        nextMenuItem.setOnAction { event ->
            if (directoryTable.getSelectionModel().getSelectedItem() != null) {
                var checkifDirectory: Boolean? = directoryTable.getSelectionModel().getSelectedItem().fileName?.isDirectory()
                // ask professor on how to use boolean? vars
                if(checkifDirectory == true && directoryTable.getSelectionModel().getSelectedItem() != null) {
                    var fileSelected = directoryTable.getSelectionModel().getSelectedItem()
                    resetDirectoryTable(fileSelected?.fileName)
                }
            }
        }

        prevMenuItem.setOnAction { event ->
            resetDirectoryTable(dir.getParentFile())
        }

        renameMenuItem.setOnAction { event ->
            if (directoryTable.getSelectionModel().getSelectedItem() != null) {
                setRenameFile()
            }
        }

        moveMenuItem.setOnAction { event ->
            if (directoryTable.getSelectionModel().getSelectedItem() != null) {
                setMoveFile()
            }
        }

        deleteMenuItem.setOnAction { event ->
            if (directoryTable.getSelectionModel().getSelectedItem() != null) {
                setDeleteFile()
            }
        }




        directoryTable.setOnKeyPressed { e ->
            if (e.getCode() === KeyCode.UP) {
                setFileDisplay()
            } else if (e.getCode() === KeyCode.DOWN) {
                setFileDisplay()
            } else if (e.getCode() === KeyCode.ENTER) {
                var checkifDirectory: Boolean? = directoryTable.getSelectionModel().getSelectedItem().fileName?.isDirectory()
                if(checkifDirectory == true) {
                    resetDirectoryTable(directoryTable.getSelectionModel().getSelectedItem().fileName)
                }
            } else if (e.getCode() === KeyCode.BACK_SPACE) {
                resetDirectoryTable(dir.getParentFile())
            } else if (e.getCode() === KeyCode.DELETE){
                resetDirectoryTable(dir.getParentFile())
            }
        }



    }
    fun setModel(model: Model) {
        this.model = model
    }

    @FXML
    private fun handleButtonAction(event: ActionEvent) {
        label.text = "Button event handled"
        model?.setValue("Model updated from Controller")
        println(model?.getValue())
    }


    @FXML
    private fun nextDirectoryClick(event: ActionEvent) {
        var checkifDirectory: Boolean? = false
        if(directoryTable.getSelectionModel().getSelectedItem() != null) {
            checkifDirectory = directoryTable.getSelectionModel().getSelectedItem().fileName?.isDirectory()
        }
        // ask professor on how to use boolean? vars
        if(checkifDirectory == true && directoryTable.getSelectionModel().getSelectedItem() != null) {
            var fileSelected = directoryTable.getSelectionModel().getSelectedItem()
            resetDirectoryTable(fileSelected?.fileName)
        }

    }

    @FXML
    private fun prevDirectoryClick(event: ActionEvent) {
        resetDirectoryTable(dir.getParentFile())
    }

    @FXML
    fun homeButtonClick(event: ActionEvent) {
        resetDirectoryTable(File("${System.getProperty("user.dir")}/test/"))
    }

    @FXML
    fun renameFileClick(event: ActionEvent) {
        if (directoryTable.getSelectionModel().getSelectedItem() != null) {
            setRenameFile()
        }
    }

    @FXML
    fun moveFileClick(event: ActionEvent) {
        setMoveFile()
    }

    @FXML
    fun deleteFileClick(event: ActionEvent?) {
        setDeleteFile()
    }

    fun deleteDirectoryHelper(directoryToBeDeleted: File): Boolean {
//        Java has an option to delete a directory. However, this requires the directory to be empty.
//        So, we need to use recursion to delete a particular non-empty directory:
//
//        Get all the contents of the directory to be deleted
//        Delete all children that are not a directory (exit from recursion)
//        For each subdirectory of current directory, start with step 1 (recursive step)
//        Delete the directory

        // IN JAVA WE COULD JUST SAY ARRAY IS NULL, IN KOTLIN MUST MAKE IT NULLABLE WITH ?
        val allContents: Array<File>? = directoryToBeDeleted.listFiles()
        if (allContents != null) {
            for (file in allContents) {
                deleteDirectoryHelper(file)
            }
        }
        return directoryToBeDeleted.delete()
    }

    private fun setDeleteFile() {
        if (directoryTable.getSelectionModel().getSelectedItem() != null) {
            //Confirmation end
            val stage = Stage()
            val valueLabel = Label("Are You Sure You Want To Delete This File?")

            valueLabel.setStyle("-fx-text-fill: red; -fx-font-size: 20px")
            valueLabel.setWrapText(true);
            valueLabel.setTextAlignment(TextAlignment.CENTER);

            val cancelButton = Button("Cancel")

            val confirmButton = Button("Confirm")

            val hbxButtons = HBox(cancelButton, confirmButton)

            cancelButton.setOnAction { event ->
                stage.close()
            }

            confirmButton.setOnAction { event ->
                var checkifDirectory: Boolean = false
                if(directoryTable.getSelectionModel().getSelectedItem()?.fileName != null) {
                    // WE NEED THE !! WHICH IS A NON NULL ASSERTION, OTHERWISE GET BELOW ERROR:
                    // Smart cast to 'String' is impossible, because 'directoryTable.getSelectionModel().getSelectedItem().fileName?.name' is a complex expression
                    checkifDirectory = directoryTable.getSelectionModel().getSelectedItem().fileName!!.isDirectory()
                }
                if(checkifDirectory) {
                    if(directoryTable.getSelectionModel().getSelectedItem().fileName != null) {
                        var deletedDirectory: File = File(directoryTable.getSelectionModel().getSelectedItem()!!.fileName!!.getAbsolutePath())
                        deleteDirectoryHelper(deletedDirectory)
                    }

                } else {
                    directoryTable.getSelectionModel().getSelectedItem().fileName?.delete()
                }
                fileViewPane.getChildren().clear();
                resetDirectoryTable(dir)
                stage.close()
            }

            hbxButtons.setSpacing(50.0)

            hbxButtons.setAlignment(Pos.CENTER);

            stage.apply {
                title = "Confirmation"
                scene = Scene(VBox(
                    valueLabel,
                    hbxButtons,
                ).apply {
                    spacing = 10.0
                }, 320.0, 150.0)
            }.show()

            stage.setResizable(false)
            stage.show()
            // end of confirmation window
        }
    }

    private fun setMoveFile() {
        if (directoryTable.getSelectionModel().getSelectedItem() != null) {

            try {

                val stage = Stage()
                val fileChooser = DirectoryChooser()
                fileChooser.setInitialDirectory(dir);
                fileChooser.setTitle("Pick Location to Move File")
                val selectedFile: File = fileChooser.showDialog(stage)
                if (selectedFile != null) {
                    var fileToMove: String? = directoryTable.getSelectionModel().getSelectedItem().printablename


                    var addExtensionFile: File

                    var customIsDirectory: Boolean? = fileToMove?.contains("/")

                    var nameOfDirectory: String = ""

                    if(directoryTable.getSelectionModel().getSelectedItem().fileName?.name != null) {
                        // WE NEED THE !! WHICH IS A NON NULL ASSERTION, OTHERWISE GET BELOW ERROR:
                        // Smart cast to 'String' is impossible, because 'directoryTable.getSelectionModel().getSelectedItem().fileName?.name' is a complex expression
                        nameOfDirectory = directoryTable.getSelectionModel().getSelectedItem().fileName!!.name
                    }

                    if(customIsDirectory == true) {
                        if(selectedFile.toString().contains(nameOfDirectory)) {
                            sendErrorMessage("Error: Can't Move Directory into Itself or Children")
                        } else {
                            addExtensionFile = File(selectedFile.toString() + "\\" + nameOfDirectory)
                            println(addExtensionFile)

                            if (addExtensionFile.exists() != true) {
                                fileViewPane.getChildren().clear();
                                directoryTable.getSelectionModel().getSelectedItem().fileName?.renameTo(addExtensionFile)
                                resetDirectoryTable(dir)
                            } else {
                                sendErrorMessage("Error: File Name Already Exists in Directory")
                            }
                        }
                    } else {
                        addExtensionFile = File(selectedFile.toString() + "\\" + fileToMove)

                        if (addExtensionFile.exists() != true) {
                            fileViewPane.getChildren().clear();
                            directoryTable.getSelectionModel().getSelectedItem().fileName?.renameTo(addExtensionFile)
                            resetDirectoryTable(dir)
                        } else {
                            sendErrorMessage("Error: File Name Already Exists in Directory")
                        }
                    }

                }
            } catch (e: java.lang.Exception) {
                println("Quit FileChooser")
            }

        }
    }

    private fun setRenameFile() {
        if (directoryTable.getSelectionModel().getSelectedItem() != null) {


            val stage = Stage()
            val fileChooser = FileChooser()
            fileChooser.setInitialDirectory(dir);
            fileChooser.setTitle("Pick Location and New Name for Renamed File")
            val selectedFile: File = fileChooser.showSaveDialog(stage)
            if (selectedFile != null) {
                var fileExtensionName: String? = directoryTable.getSelectionModel().getSelectedItem().printablename?.split(".")?.last()
                var addExtensionFile: File

                var customIsDirectory: Boolean? = fileExtensionName?.contains("/")

                if(customIsDirectory == true) {
                    addExtensionFile = File(selectedFile.toString())
                } else {
                    addExtensionFile = File(selectedFile.toString() + "." + fileExtensionName)
                }

                if (addExtensionFile.exists() != true) {
                    fileViewPane.getChildren().clear();
                    directoryTable.getSelectionModel().getSelectedItem().fileName?.renameTo(addExtensionFile)
                    resetDirectoryTable(dir)
                } else {
                    sendErrorMessage("Error: File Name Already Exists in Directory")
                }

            }

        }
    }

    private fun sendErrorMessage(errorMessage: String) {
        val stage = Stage()
        val valueLabel = Label(errorMessage)

        valueLabel.setStyle("-fx-text-fill: red; -fx-font-size: 20px")
        valueLabel.setWrapText(true);
        valueLabel.setTextAlignment(TextAlignment.CENTER);

        stage.apply {
            title = "Error message"
            scene = Scene(VBox(
                valueLabel,
            ).apply {
                spacing = 10.0
            }, 320.0, 150.0)
        }.show()

        stage.setResizable(false)
        stage.show()

    }

    private fun setFileDisplay() {
        var fileSelected = directoryTable.getSelectionModel().getSelectedItem()

        fileViewPane.getChildren().clear();

        directoryPathLabel.setText(fileSelected.fileName.toString())

        // needs to be declared as boolean? otherwise if statement gets confused
        var b: Boolean? = fileSelected.printablename?.contains(".")

        if(b == true) {
            // gets file extension
            var fileExtensionName: String? = fileSelected?.printablename?.split(".")?.last()
            if((fileExtensionName == "png" || fileExtensionName == "jpg") || fileExtensionName == "bmp") {
                val stream: java.io.InputStream = FileInputStream(fileSelected.fileName)
                val image = Image(stream)
                //Creating the image view
                val imageView = ImageView()
                //Setting image to the image view
                imageView.setFitHeight(fileViewPane.prefHeight - 15.0);
                imageView.setImage(image)
                imageView.setPreserveRatio(true)
                val imageScroll = ScrollPane()
                imageScroll.setPrefViewportWidth(450.0);
                imageScroll.setPrefViewportHeight(fileViewPane.prefHeight - 15.0);
                imageScroll.setContent(imageView)
                fileViewPane.getChildren().add(imageScroll)
                // STREAM IS VERY IMPORTANT, IF YOU DONT CLOSE RENAMING IT DOESNT WORK CAUSE IT NEVER CLOSED
                stream.close()

            } else if (fileExtensionName == "txt" || fileExtensionName == "md") {
                var text = ""

                val textArea = TextArea()
                textArea.setWrapText(true)
                textArea.setEditable(false)
                textArea.setMaxWidth(fileViewPane.minWidth)
                textArea.setMinHeight(fileViewPane.prefHeight)

                val file: File? = fileSelected.fileName
                try {
                    Scanner(file).use { input ->
                        while (input.hasNextLine()) {
                            text = text + input.nextLine()
                        }
                        textArea.setText(text);
                        fileViewPane.getChildren().add(textArea)
                    }
                } catch (ex: FileNotFoundException) {
                    ex.printStackTrace()
                }
            } else {
                var notSupported : Label = Label()
                notSupported.setText("Unsupported type")
                fileViewPane.getChildren().add(notSupported)
            }
        } else {
            var notSupported : Label = Label()
            notSupported.setText("This is a Directory")
            fileViewPane.getChildren().add(notSupported)
        }
    }

    private fun resetDirectoryTable(newFile: File?) {

        // checks if we're going too far up in filepath, are we getting double slash that appear twice
        val first: Int = newFile.toString().indexOf("\\")
        val second: Int = newFile.toString().indexOf("\\", first + 1)

        if(newFile != null && second != -1) {
            dir = newFile
        }
        column1.setText("In '" + dir.name + "' Folder")
        directoryTable.getItems().clear()
        newFile?.listFiles()?.forEach {


            var namevar = it.name

            if(it.isDirectory()) {
                namevar = "$namevar/"
            }
            directoryTable.getItems().add(FileWrapper(it, namevar))
        }
    }

}